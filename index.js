const fs = require('fs');
const path = require('path');
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const { PDFDocument } = require('pdf-lib');

const argv = yargs(hideBin(process.argv)).argv

async function getDoc(source) {
  const sourceFile = path.join(source);
  const formPdfBytes = fs.readFileSync(sourceFile);
  return await PDFDocument.load(formPdfBytes)
}
function writeForm(dest, file) {
  const destFile = path.join(dest);
  fs.writeFileSync(dest, file);
}

function getAnswersFromFile(source) {
  const sourceFile = path.join(source);
  return JSON.parse(fs.readFileSync(sourceFile));
}
function getAnswersFromString(source) {
  return JSON.parse(source);
}

async function main() {
  const doc = await getDoc(argv.source);
  const form = doc.getForm();

  if(argv.fields) {
    console.dir(form.getFields().map( f => f.getName() ));
    return;
  }

  let answers = {};
  let demo_mode = false;
  if(argv['with-file'] !== undefined) {
    answers = getAnswersFromFile(argv['with-file']);
  } else if( argv['with-json'] !== undefined ) {
    answers = getAnswersFromString(argv['with-json']);
  } else if( argv['with-demo'] !== undefined ) {
    demo_mode = true;
  } else {
    console.error("ERROR - No answers were provided");
    return;
  }

  const unanswered_questions = [];
  const used_answers = [];

  form.getFields().forEach( f => {
    let field_name = f.getName();
    if( answers[field_name] !== undefined ) {
      f.setText(answers[field_name]);
      used_answers.push(field_name);
    } else if(demo_mode) {
      f.setText('dummy text');
    } else {
      unanswered_questions.push(field_name);
    }
  });

  const unused_answers = Object.keys(answers).filter( a => !used_answers.includes(a) );

  if( unanswered_questions.length !== 0 ) {
    console.log('WARNING - There are questions that were not answered:');
    console.dir(unanswered_questions);
  }

  if( unused_answers.length !== 0 ) {
    console.log('WARNING - There are answers provided that were not used:');
    console.dir(unused_answers);
  }

  form.flatten();
  const pdfBytes = await doc.save();
  if( argv.output !== undefined ) {
    writeForm(argv.output, pdfBytes);
  } else {
    console.error("ERROR - No output file was specified")
    return;
  }

}
main().then( () => {
  console.log("done");
});